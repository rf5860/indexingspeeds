const _ = require('lodash');
const chart = require('chart.js');
const prettyMs = require('pretty-ms');

window.onload = () => {
    const data = {};
    const context = document.getElementById('indexing').getContext('2d');
    const text = document.getElementById('data').innerText;
    const allLines = text.split(/\n/g).slice(0, -1);
    const rows = allLines.map(line => line.split(','));
    const [header, ...paddedLines] = rows;
    const lines = paddedLines.map(line => line.map(field => field.trim()));
    const {
        NORMAL,
        IGNORE
    } = _.groupBy(lines, 2);
    data.labels = _.uniq(lines.map(line => `${line[0]} x ${line[1]}`));
    data.datasets = [{
        label: 'Normal',
        backgroundColor: '#6b48ff',
        data: _.map(NORMAL, 3)
    }, {
        label: 'Ignored',
        backgroundColor: '#ff8c00',
        data: _.map(IGNORE, 3)
    }];

    new Chart(context, {
        type: 'bar',
        data: data,
        options: {
            barValueSpacing: 15,
            tooltips: {
                callbacks: {
                    afterLabel: (i, t) => prettyMs(i.yLabel)
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0.1,
                        callback: prettyMs
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Running time (2,000 records)'
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Batch Size x Fetch Size'
                    }
                }]
            }
        }
    });
};